#!/bin/bash

test -d /etc/docker-start.d && echo --- Running /etc/docker-start.d/ --- && run-parts /etc/docker-start.d
# Start cron (background process)
if [ $(find /etc/periodic/ -type f|grep -v '.dir' |wc -l) -gt 0  ] ; then 
  [ $(id -u) -eq 0 ] && test -e /usr/sbin/crond && echo "--- Starting crond ---" || echo "--- Skipping cron ---"
  [ $(id -u) -eq 0 ] && test -e /usr/sbin/crond && crond
fi
echo Nginx user:
id nginx
# Start PHP-FPM (background process)
echo "--- Starting php ---"
php-fpm

# Start NGINX (foreground process)
echo "--- Starting nginx ---"
/usr/sbin/nginx -g "daemon off;"
echo "--- nginx stopped ---"
