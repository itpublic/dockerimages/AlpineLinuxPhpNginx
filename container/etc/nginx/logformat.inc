# UiB log format
# this mus be copied or linked into nginx.conf
log_format  main  '$remote_addr - $remote_user [$time_iso8601] "$request" '
                  '"$status" $body_bytes_sent "$http_referer" '
                  '"$http_user_agent" '
                  'forwarded_for "$http_x_forwarded_for" '
                  'gzip_ratio $gzip_ratio '
                  'upstream_response_time $upstream_response_time '
                  'request_time $request_time '
                  'upstream_addr $upstream_addr '
                  'hit_nr $sent_http_X_Cache_Hits '
                  'cache $sent_http_X_Cache '
                  'content_type $sent_http_Content_Type '
                  'hostname $http_host ';
