rewrite ^/favicon.ico$ https://shared.app.uib.no/favicon.ico permanent;
rewrite ^/apple-touch-icon.png$ https://shared.app.uib.no/img/uib-logo-120x120_whiteBackground_withPadding.png permanent;
rewrite ^/apple-touch-icon-120x120.png$ https://shared.app.uib.no/img/uib-logo-120x120_whiteBackground_withPadding.png permanent;
