location = /robots.txt {
    access_log off;
    log_not_found off;
    return 200 "User-agent: *\nDisallow: /";
}