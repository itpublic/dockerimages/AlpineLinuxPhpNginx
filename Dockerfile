FROM alpine:3.21

# PHP version without "."
# Note, remember to move configfiles in container/etc/...
ARG PHP_VER=84

# Refresh cache and upgrade pagages
RUN apk update && apk --no-cache upgrade

# Add PHP generics
RUN apk add --no-cache \
    bash \
    ca-certificates \
    git \
    gzip \
    nginx \
    openssl \
    php${PHP_VER} \
    php${PHP_VER}-apcu \
    php${PHP_VER}-curl \
    php${PHP_VER}-exif \
    php${PHP_VER}-fpm \
    php${PHP_VER}-gd \
    php${PHP_VER}-json \
    php${PHP_VER}-mbstring \
    php${PHP_VER}-opcache \
    php${PHP_VER}-openssl \
    php${PHP_VER}-pdo \
    php${PHP_VER}-pdo_pgsql \
    php${PHP_VER}-phar \
    php${PHP_VER}-session \
    php${PHP_VER}-zlib \
    unzip \
    && apk cache clean


# php${PHP_VER} symlinks
RUN ln -s /usr/sbin/php-fpm${PHP_VER} /usr/bin/php-fpm && \
    ln -s /usr/lib/php${PHP_VER} /usr/lib/php

RUN mkdir -p /var/log/php-fpm /run/php-fpm/ /run/nginx
RUN chown -R nginx /var/log/php-fpm

RUN test -e /usr/bin/php || ln  -vs /usr/bin/php${PHP_VER} /usr/bin/php

# Redirect logs to stdout/stderr
RUN ln -s /proc/1/fd/1 /var/log/php-fpm/www-slow.log
RUN ln -s /proc/1/fd/2 /var/log/php-fpm/www-error.log
RUN ln -s /proc/1/fd/2 /var/log/php${PHP_VER}/error.log
RUN ln -s /proc/1/fd/1 /var/log/nginx/access.log
RUN ln -s /proc/1/fd/2 /var/log/nginx/error.log

# Make some default directories
RUN mkdir -p /opt/composer && \
    mkdir -p /var/www/site/htdocs

# Composer install
# Note: Alpine 3.19.0 did not support composer install via package
# with PHP8.3. Install directly from composer in order to avoid this
# in the future.
# https://gitlab.alpinelinux.org/alpine/aports/-/issues/15554
WORKDIR /opt/composer

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    php composer-setup.php && \
    php -r "unlink('composer-setup.php');" && \
    ln -s /opt/composer/composer.phar /usr/bin/composer

WORKDIR /var/www

# Add container config
COPY --chown=root:root ./container/etc /etc

# Copy standard version of start.sh, but not run.
COPY container/start.sh /start.sh
RUN chmod a+x /start.sh

