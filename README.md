# Alpine Linux PHP-FPM & Nginx containter

This container gives a template/start for running PHP applications.

## TOC

[[_TOC_]]

## Versioning

Versioning of the containers are in the format of x.x.x

The two first numbers corresponds with the Alpine Linux numbers.
The third and last number is a running number.

| Version | Software | EOL        |
| ------- | -------- | ---------- |
| 3.21    | PHP 8.4  | 2026-11-01 |
| 3.20    | PHP 8.3  | 2026-04-01 |
| 3.19    | PHP 8.3  | 2025-11-01 |
| 3.18    | PHP 8.2  | 2025-05-09 |
| 3.17    | PHP 8.1  | 2024-11-22 |

Other tags:
* **latest**: This tag follows the master branch, should work most of the time.
* **beta**: This tag follows the beta branch, and should be assumed to be unstable. Used for testing new versjons. The tag may be removed by cleanup script.

## Changes
### Version 3.21

* Nginx-workers is now a static number (`/etc/nginx/conf.d/workers.conf`)
* Nginx log format now set in (`/etc/nginx/http.d/00_logformat.conf`)
* Nginx absolute redirect now off by default (`/etc/nginx/http.d/00_absolute_redirect.conf`) (v3.21.3)
* A [Kubernetes/RAIL compatible container](https://git.app.uib.no/itpublic/dockerimages/alpinelinuxphpnginx-unprivileged) will be buid on top of this image
* The default vhost no longer as an inline robots.txt. Use `RUN sed -i 's:#INCLUDES#:#INCLUDES#\n   include nginx/default.d/robots.txt_block.inc;:g' /etc/nginx/http.d/default.conf` to get it back.

### Version 3.19

* PHP-fpm now listen to port 9000, and not unix:/var/run/php-fpm/www.sock;
* No chmod on boot of /etc/docker-start.d/*.sh

# Example of use
- [rom.app.uib.no](https://git.app.uib.no/it/site/rom.app.uib.no/)
- [oppetid.app.uib.no](https://git.app.uib.no/it/site/oppetid.app.uib.no)

# How to use

## Configure Nginx

You can do some changes Nginx without editing the nginx.conf file.

The max size of post request 
`container/etc/nginx/http.d/0_client_max_body_size.conf`
```
client_max_body_size 100m;
```

To change number of workers, you must overwrite `/etc/nginx/conf.d/workers.conf`
```
# worker_processes auto; = set to number of CPUs (not recommended).
worker_processes 3;
```

## Configure PHP

To append php-fpm config, add a file to the php-fpm.d dir. Use files that begins with z, so that it is loaded after the existing files.

Timezone example `container/etc/php83/php-fpm.d/z01_timezone.conf`

```
[www]
php_admin_value[date.timezone] = Europe/Oslo

```

Workers example `container/etc/php83/php-fpm.d/z01_workers.conf`
```
[www]
pm = dynamic
; As per HTTP1.1 standard, a browser open 6 max connection per server.
pm.min_children = 2
pm.max_children = 20

; Prevent jojo-ing of processes when pm = dynamic is used with low min_children.
pm.process_idle_timeout = 600s

; Memory limit
php_admin_value[memory_limit] = 1024M
```

Add the file in Dockerfile.

```
COPY --chown=root:root ./container/etc /etc
```


## Pre-run scripts

You can place scipts in /etc/docker-start.d if you need some commands to
run before the services starts.

## Docker file examples

### Bare-bone

```
FROM git.app.uib.no:4567/itpublic/dockerimages/alpinelinuxphpnginx:latest
EXPOSE 80
RUN apk update && apk --no-cache upgrade
COPY --chown=root:nginx ./ /var/www/site
CMD ["/bin/sh", "/start.sh"]

```

### Site running Smarty via composer
```
FROM git.app.uib.no:4567/itpublic/dockerimages/alpinelinuxphpnginx:latest
#FROM git.app.uib.no:4567/itpublic/dockerimages/alpinelinuxphpnginx:3.17

# Open webport 80
EXPOSE 80

# Refresh cache and upgrade pagages
RUN apk update && apk --no-cache upgrade

# Add extra pagages
RUN apk add --no-cache lynx bash

# Add application
# Note: Use .dockerignore to exclude files,
# or run dir for dir like: COPY --chown=root:nginx ./htdocs /var/www/site/htdocs
COPY --chown=root:nginx ./ /var/www/site
# Make cache dirs
RUN mkdir -p /var/www/site/smarty/cache && chown nginx /var/www/site/smarty/cache &&\
    mkdir -p /var/www/site/smarty/templates_c && chown nginx /var/www/site/smarty/templates_c

WORKDIR /var/www/site

# Install via Composer
RUN if [ -f composer.json ] ;then composer config discard-changes true ;fi
RUN if [ -f composer.json ] ;then composer install -n --no-dev ;fi

# Install other applications
RUN apk add --allow-untrusted https://example.com/foo/bar.apk

# Appending fastcgi_params
RUN echo "# Appending config file" >> /etc/nginx/fastcgi_params && \
    echo "fastcgi_connect_timeout 123s;" >> /etc/nginx/fastcgi_params && \
    echo "fastcgi_read_timeout 123s;" >> /etc/nginx/fastcgi_params && \
    echo "fastcgi_send_timeout 123s;" >> /etc/nginx/fastcgi_params


# Add container config
COPY --chown=root:root ./container/etc /etc

# Starter nginx og php-fpm
CMD ["/bin/sh", "/start.sh"]

```

### Non-root container

Some container host system demands non-root containers. It is not posible to run crond in a non-root container due to the need to change user when running.

[non-root sample](samples/non-root)

```
FROM git.app.uib.no:4567/itpublic/dockerimages/alpinelinuxphpnginx:latest
EXPOSE 80
RUN apk update && apk --no-cache upgrade

COPY --chown=root:root ./ /var/www/site
# If application needs to write
RUN chown -R nginx /var/www/site/cache_dir

# Enshure container runs as nginx-user.
USER 100
CMD ["/bin/sh", "/start.sh"]
```