#!/bin/bash
#####################################################################
# Intended to test changes in a local docker container before       #
# pushing to git.                                                   #
#####################################################################

usage="$0 [options]

-b / --build-only: Do not run the container, only start it
-p / --plain-log:  Output build progress as plain text
-h / --help:       Print this message

"

name="nonroot-sample"
containerPort=8080

echo "Container name: $name"

buildOnly=0
mount=0
cache=' '
logFormat=''
# Handle input vars
while [[ $1 = -* ]]; do
    arg=$1; shift           # shift the found arg away.

    case $arg in
        -c|--no-cache)
            cache=' --no-cache '
            ;;
        -b|--build-only)
            buildOnly=1
            ;;
        -p|--plain-log)
            logFormat=' --progress=plain '
            ;;
        -m|--mount)
            mount=1
            ;;
        -h|--help)
            echo -e "$usage";
            exit
            ;;
    esac
done

if [ ! -f .env ] ; then
  echo No .env file
fi
# Local .env

if [[ -n $DOCKER_PUBLISH_PORT ]] 2>/dev/null ;then
    IPport="${DOCKER_PUBLISH_PORT}"
else
    IPport=8000;
fi


# Delete old and build new container
docker ps
docker stop $name 2>/dev/null
docker rm $name 2>/dev/null
docker build . --network host $logFormat --tag $name

#Find a free port starting at $IPport
for i in $(seq $IPport 8999) ;do
	if [ $(netstat -tulpn 2>/dev/null |grep LISTEN |grep tcp| grep ":$i"|wc -l) -lt 1 ] ;then
		IPport=$i
		break
	fi
done

echo Port: $IPport
netCmd=" --publish 127.0.0.1:${IPport}:${containerPort} "

shared_volumes=" -v $(pwd)/.env:/.env:ro "
env=''

echo $shared_volumes | sed 's: -v :\n-v :g'

echo "#================================================="
echo -n "docker run -d "
echo $shared_volumes | sed 's: -v :\n  -v :g'
if [ "$env" != "" ] ;then echo $env |  sed 's: -e :\n  -e :g' ;fi
echo "  --name $name $netCmd $name"
echo "#================================================="

if [ $buildOnly -eq 1 ] ;then
  echo "Script run in build only mode"
else
  docker run -d $shared_volumes $env --name $name $netCmd $name
  docker ps
  echo "#==============[Tool tip]========================="
  echo "Terminal      : docker exec -it $name bash"
  echo "Terminal      : docker logs -f --since=5m $name"
  echo "URI:          : http://localhost:$IPport/"
fi
