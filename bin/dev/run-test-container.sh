#!/bin/bash
# This script is intended for local of container
# Note /start.sh is disabled by default, must run it as "manually" at start

#Adding defaults
shared_volumes=" "
netCmd=" "
name="uibalpine"
# Help string
usage="$0 [options]

       -c / --no-cache:  Disable cache on build
       -q / --quiet:     No output on build, exept errors
       -p / --plain:     Use plain output on build

uibalpine_index.php in PWD is mounted into container if file exists.
"

# Getting args
buildvars=' '
# Handle input vars
while [[ $1 = -* ]]; do
    arg=$1; shift           # shift the found arg away.
    case $arg in
        -c|--no-cache)
            buildvars="$buildvars --no-cache"
            ;;
        -q|--quiet)
            buildvars="$buildvars --quiet"
            ;;
        -p|--plain)
            buildvars="$buildvars --progress plain"
            ;;
        -h|--help)
            echo -e "$usage";
            exit
            ;;
    esac
done

# Stop and remove container
docker rm -f $name
# Build container
docker build $buildvars --tag $name .
#Start free port search at:
IPport=8000
[[ "$DOCKER_PUBLISH_PORT" =~ ^[0-9]+$ ]] && IPport=$DOCKER_PUBLISH_PORT
#Find a free port starting at $IPport
for i in $(seq $IPport 8999) ;do
	if [ $(netstat -tulpn 2>/dev/null |grep LISTEN |grep tcp| grep ":$i"|wc -l) -lt 1 ] ;then
		echo $i
		IPport=$i
		break
	fi
done
# Start the container
echo Port: $IPport
netCmd=" --publish 127.0.0.1:${IPport}:80 "
test -f $(pwd)/.env && shared_volumes=" -v $(pwd)/.env:/.env:ro "
# Adding a test-php script
test -f $(pwd)/uibalpine_index.php && shared_volumes="$shared_volumes -v $(pwd)/uibalpine_index.php:/var/www/site/htdocs/index.php"

echo runing: docker run --name $name $netCmd $shared_volumes  $name /start.sh
docker run -d --name $name $netCmd $shared_volumes $name /start.sh || exit
docker ps
echo "#==============[Tool tip]=========================="
echo "Terminal      : docker exec -it $name bash"
echo "Terminal      : docker logs -f --since=5m $name"
echo "URI:          : http://localhost:$IPport/"
echo "#=================================================="
